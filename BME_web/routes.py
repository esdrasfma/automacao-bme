from BME_web import app
from flask import render_template, request


@app.route('/')
def index():
    return render_template("index.html", title="Página inicial")


@app.route('/instrumentos')
def instrumentos():
    return render_template("instrumentos.html", title="Instrumentos")


@app.route('/operar_instrumentos', methods=["GET"])
def operar_instrumentos():
    #todo
    return render_template("instrumentos.html", title="Instrumentos")


@app.route('/instrumento_34411a')
def instrumento_34411a():
    return render_template("instrumento_34411a.html", title="Instrumento 34411A")


@app.route('/instrumento_6030a')
def instrumento_6030a():
    return render_template("instrumento_6030a.html", title="Instrumento 6030A")


@app.route('/como_conectar_34411a')
def como_conectar_34411a():
    return render_template("como_conectar_34411a.html", title="Como conectar 34411A")


@app.route('/como_conectar_6030a')
def como_conectar_6030a():
    return render_template("como_conectar_6030a.html", title="Como conectar 6030A")
