function atualizar_cards() {
    if (desktop) {
        $('[name="Utilização"]').attr('data-aos-delay', 50);
        $('[name="API"]').attr('data-aos-delay', 100);
        $('[name="Guias"]').attr('data-aos-delay', 150);
    }else {
        $('[name="Utilização"]').attr('data-aos-delay', 0);
        $('[name="API diversos"]').attr('data-aos-delay', 0);
        $('[name="Guias"]').attr('data-aos-delay', 0);
    }
}

$(document).ready(function () {
    atualizar_cards();
    AOS.init();
})

window.addEventListener('resize', atualizar_cards);

$('[name="botao-operar"]').click(function() {
    alert('Essa opção ainda está em desenvolvimento e estará pronta no futuro!')
});
