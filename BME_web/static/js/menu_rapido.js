function atualizar_menurapido() {
   if (!desktop) {
      $('#menurapidoCollapse').removeClass('show');
   } else {
      $('#menurapidoCollapse').addClass('show');
   }
}

function fixar_menurapido_se_necessario() {
   if(desktop) {
      var currentScroll = $(window).scrollTop();
      if (currentScroll >= 70) {
         $('#menurapido').addClass('menurapido-fixed');
      } else {
         $('#menurapido').removeClass('menurapido-fixed');      
      }
   } else {
      $('#menurapido').removeClass('menurapido-fixed');      
   }
}

function mostrar_voltar_fimdapagina_se_necessario() {
   if (!desktop) {
      AOS.refreshHard();
      $('#voltar_fimdapagina').removeClass('d-none');
   } else {
      $('#voltar_fimdapagina').addClass('d-none');
   }
}

$(window).scroll(function() {
   fixar_menurapido_se_necessario();
});

window.addEventListener('resize', atualizar_menurapido);
window.addEventListener('resize', fixar_menurapido_se_necessario);
window.addEventListener('resize', mostrar_voltar_fimdapagina_se_necessario);

$(document).ready(function() {
   atualizar_menurapido();
   fixar_menurapido_se_necessario();
   mostrar_voltar_fimdapagina_se_necessario();
});
