var desktop;

function detect_if_desktop() {
    if (window.matchMedia("(max-width: 992px)").matches) {
        desktop = false;
    } else {
        desktop = true;
    }
}
detect_if_desktop();

window.addEventListener('resize', detect_if_desktop);
